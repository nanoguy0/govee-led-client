# Govee Bluetooth LED Client
This NodeJS library allows the user to control their `Govee` LED lights using the noble bluetooth libraries. This works for windows, linux and darwin thanks to the `noble-winrt` implementation of `noble` for windows.

# Installation

This library requires `noble` in order to work, if you have a windows computer use `noble-winrt` .

`npm install noble govee-led-client`

# Example

```javascript
const Noble = require("noble");
// const Noble = require("noble-winrt"); // If you use windows, use this library
const BluetoothLED = require('govee-led-client');

var Client = new BluetoothLED('<mac-address>', Noble)


Client.on('connected', () => {
  console.log('Connected');

  Client.setState(true); // Turn on the LED
  Client.setState(false); // Turn off the LED

  Client.setColor('red'); // Use Color Strings
  Client.setColor('#ff0000'); // Use Hex colors
  Client.setColor('rgb(123, 234, 45)'); // Use Color Definitions

  Client.setBrightness(0); // Set the brightness at 0%
  Client.setBrightness(100); // Set the brightness at 100%

  Client.disconnect(); // Disconnect from the device

});

// The device is disconnected and will NOT attempt to reconnect
Client.on('disconnect', () => console.log('Disconnected')); 
// If this was not client initiated, the device will attempt to reconnect
Client.on('ble:disconnect', () => console.log('Disconnected')); 
// The client successfully reconnected
Client.on('reconnected', () => console.log('Reconnected'))
```

# Notes

- In order to get the mac address of the device you need to connect to, you can connect to it on your phone and use wireshark or something of the sort to sniff the mac, or you can connect to it on your computer and lookup the mac from there. The device was called `ihommet_XXXX_XXX` for myself.
# Credits

- [yetzt/node-rgbcolor](https://github.com/yetzt/node-rgbcolor) For the color parser snippet
- [Freemanium/govee_btled](https://github.com/Freemanium/govee_btled) For reverse enginering the LED Bluetooth API.
- [Noble BLE Lib](https://github.com/noble/noble) For allowing this package to use the bluetooth APIs.